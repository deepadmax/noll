# Noll

**Nothing. Nil. Nada.**

An object that is as not there as I could conjure up.

You can safely index it and compare with it. It will always return an untruthy value, whether that be `0`, `""`, or itself whenever possible.

```py
from noll import Noll

Noll().index.whatever['and'].you.will.always.end.up['with'].a.Noll['object']
```
